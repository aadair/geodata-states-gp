CREATE PROCEDURE dbo.insert_lyr_prc
	@mxd_path NVARCHAR(500) = NULL,
	@layer_index INT = 0,
	@sub_folder NVARCHAR(50) = NULL,
	@layer_name NVARCHAR(50) = NULL,
	@gp_service_url NVARCHAR(500) = NULL
	AS
	BEGIN
		SET NOCOUNT ON;
		INSERT INTO dbo.webFeatureServicesList
		(
			mxd_path,
			layer_index,
			sub_folder,
			layer_name,
			gp_service_url
		)
		VALUES
		(
			@mxd_path,
			@layer_index,
			@sub_folder,
			@layer_name,
			@gp_service_url
		);
	END
GO

CREATE PROCEDURE dbo.clear_by_gp_service
	@gp_service_url NVARCHAR(500) = NULL
	AS
	BEGIN
		SET NOCOUNT ON;
		DELETE FROM dbo.webFeatureServicesList
		WHERE gp_service_url = @gp_service_url;
	END
GO
