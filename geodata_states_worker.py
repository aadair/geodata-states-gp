"""
Worker script for refreshing available datasets.

"""
MXD_ROOT = r'\\p119lagunap401\arcgissystem\arcgisinput'
DSN = r'DRIVER={SQL Server};SERVER=P119COLLINSP402\NGMC_DEV;DATABASE=nrcsstatesmart;UID=sa;PWD=usda*gdw1'
GP_SVC_URL = 'http://gistest1.ftw.nrcs.usda.gov/arcgis/rest/services/GeoDataStates_DataExtract/GPServer/Data%20extract'
PLACES = {'ak': 'Alaska',
          'al': 'Alabama',
          'ar': 'Arkansas',
          'as': 'American Samoa',
          'az': 'Arizona',
          'ca': 'California',
          'co': 'Colorado',
          'de': 'Delaware',
          'fl': 'Florida',
          'fm': 'Federated States of Micronesia',
          'ga': 'Georgia',
          'gu': 'Guam',
          'hi': 'Hawaii',
          'ia': 'Iowa',
          'id': 'Idaho',
          'il': 'Illinois',
          'in': 'Indiana',
          'ks': 'Kansas',
          'ky': 'Kentucky',
          'la': 'Louisiana',
          'ma': 'Massachusetts',
          'md': 'Maryland',
          'me': 'Maine',
          'mh': 'Marshall Islands',
          'mi': 'Michigan',
          'mn': 'Minnesota',
          'mo': 'Missouri',
          'mp': 'Mariana Islands',
          'ms': 'Mississippi',
          'nc': 'North Carolina',
          'ne': 'Nebraska',
          'nh': 'New Hampshire',
          'nj': 'New Jersey',
          'nm': 'New Mexico',
          'nv': 'Nevada',
          'ny': 'New York',
          'oh': 'Ohio',
          'ok': 'Oklahoma',
          'or': 'Oregon',
          'pa': 'Pennsylvania',
          'pr': 'Puerto Rico',
          'pw': 'Palau',
          'sc': 'South Carolina',
          'sd': 'South Dakota',
          'tn': 'Tennessee',
          'tx': 'Texas',
          'ut': 'Utah',
          'va': 'Virginia',
          'vt': 'Vermont',
          'wa': 'Washington',
          'wi': 'Wisconsin',
          'wv': 'West Virginia',
          'wy': 'Wyoming',
          'usa': 'USA'}

from arcpy.mapping import ListDataFrames, ListLayers, MapDocument
from glob import iglob
from os import walk
from os.path import normpath, sep
import pyodbc


def mxds(search_path):
    """
    Yields paths to MXD files.

    """
    for root, dirs, files in walk(search_path):
        for mxd_path in iglob('{0}\\*.mxd'.format(root)):
            yield MapDocument(mxd_path)


def feature_layers(mxd):
    """
    Yields available layers by index and layer object from a given MXD.

    """
    for df in ListDataFrames(map_document=mxd):
        for i, lyr in enumerate(ListLayers(map_document_or_layer=mxd,
                                           data_frame=df)):
            if lyr.isFeatureLayer:
                yield i, lyr


def insert_layer(cur, mxd_path, layer_index, subfolder, layer_name,
                 gp_service_url):
    """
    Calls the stored procedure to insert a single layer.

    """
    cur.execute("exec insert_lyr_prc '{0}',{1},'{2}','{3}','{4}'".format(mxd_path,
                                                                         layer_index,
                                                                         subfolder,
                                                                         layer_name,
                                                                         gp_service_url))


def delete_layers(cur, gp_service_url):
    """
    Deletes layers for a given geoprocessing service URL.

    """
    cur.execute("exec clear_by_gp_service '{0}'".format(gp_service_url))


if __name__ == '__main__':
    with pyodbc.connect(DSN) as cnxn:
        with cnxn.cursor() as cursor:
            delete_layers(cursor, GP_SVC_URL)
            cursor.commit()
            for mxd in mxds(MXD_ROOT):
                subfolder = normpath(mxd.filePath).replace(normpath(MXD_ROOT),
                                                           '').split(sep)[1]
                subfolder = subfolder.lower()
                for i, lyr in feature_layers(mxd):
                    place = PLACES.get(subfolder)
                    if not place:
                        place = subfolder
                    layer_name = '{0} {1}'.format(place, lyr.name).strip()
                    layer_name = layer_name.replace('_', ' ')
                    insert_layer(cursor, mxd.filePath, i, subfolder,
                                 layer_name, GP_SVC_URL)
            cursor.commit()
