# geodata-states-gp

geodata_states_async.pyt is a Python toolbox designed to provide "Clip and ship" functionality for the geodata states map services.

## Installation

There is documentation provided by Esri regarding how to publish geoprocessing services, which is applicable in this case.

There are a few things to keep in mind when publishing the toolbox to ArcGIS Server.

* For layers to be available through the service, an identical local copy of all mxds needs to be visible to the toolbox when publishing from a remote machine.

* The tool will need to be republished any time map services change, and when map services are added or removed from ArcGIS Server.

* There is a layer file (aoi_polygon.lyr) that needs to be manaully pasted into the server-side deployment directory alongside the geodata_states_async.pyt file.

* Deploying the toolbox to the root service directory in ArcGIS server will expose all map data as potential data downloads, but deploying the toolbox to a sub directory restricts potential data downloads to only mxds within that ArcGIS Server sub directory.

## Usage

Once the service is up and running, there are several ways to interact with the service. Here are three ways.

### ArcGIS Desktop

* Add the toolbox from ArcGIS server to ArcToolbox.

* Open the tool, sketch an AOI, select the desired layer, and run the tool.

* Open the results dialog, and expand the process.

* Double-click the result url to download custom.zip, which contains a geodatabase.

### Web browser (ArcGIS REST API)

* Navigate to the ArcGIS Server REST enpoint for the service.

* Add a featureset as an AOI.

    Here is an example featureset as JSON:

        {
           "geometryType":"esriGeometryPolygon",
           "fields":[],
           "spatialReference":{
              "wkid":4326
           },
           "features":[
              {
                 "geometry":{
                    "rings":[
                       [
                          [
                             -98.33,
                             38.48
                          ],
                          [
                             -98.32,
                             38.52
                          ],
                          [
                             -98.28,
                             38.51
                          ],
                          [
                             -98.33,
                             38.48
                          ]
                       ]
                    ],
                    "spatialReference":{
                       "wkid":4326
                    }
                 },
                 "attributes":{}
              }
           ]
        }

* Type the name of an appropriate dataset (there is a choice list) and submit the job.

* Periodically check the status of the job. When the job is complete, a url parameter is provided. The url is the url to a zip file containing a geodatabase with the extracted data.

### WFS Loader Add-in (Ravi's tool)

TODO: Somebody else needs to write this part.
