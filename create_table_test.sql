CREATE TABLE dbo.webFeatureServicesList
	(
		mxd_path NVARCHAR(500) NULL,
		layer_index INT NULL,
		sub_folder NVARCHAR(50) NULL,
		layer_name NVARCHAR(50) NULL,
		gp_service_url NVARCHAR(500) NULL
	);
